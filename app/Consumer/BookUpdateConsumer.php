<?php

namespace Maxim\Consumer;

use Maxim\Service\Book;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class NotificationConsumer
 */
class BookUpdateConsumer implements ConsumerInterface
{
    /**
     * @var Book
     */
    protected $bookService;

    /**
     * BookUpdateConsumer constructor.
     * @param Book $bookService
     */
    public function __construct(Book $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * @var AMQPMessage $msg
     * @return void
     */
    public function execute(AMQPMessage $message)
    {
        echo 'Попытка выполнить запрос: ' . PHP_EOL;
        $this->bookService->consumeUpdate($message);
        echo 'Запрос выполнен' . PHP_EOL;
    }
}