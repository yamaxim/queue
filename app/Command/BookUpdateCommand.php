<?php
namespace Maxim\Command;
use Maxim\Service\Book;
use Maxim\Service\RedisQueue;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;


class BookUpdateCommand extends ContainerAwareCommand
{
    /**
     * Conf
     */
    public function configure()
    {
        $this
            ->setName("book-update")
            ->setDescription("This command makes worker consume events");
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()
            ->get('old_sound_rabbit_mq.book_update_producer')
            ->publish('***Содержиме запроса***' . PHP_EOL);
    }
}