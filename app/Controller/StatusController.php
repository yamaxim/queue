<?php

namespace Maxim\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Maxim\Service\Book;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;

/***
 * Class StatusController
 * @package Maxim\Controller
 */
class StatusController extends FOSRestController
{
    /**
     * @var Book
     */
    protected $book;

    /**
     * StatusController constructor.
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    /**
     * Returns a status
     * @Rest\Get("/statuses/{id}")
     * @param $id
     * @return JsonResponse
     */
    public function get($id)
    {
        return new JsonResponse($this->book->getMessageStatus($id));
    }
}