<?php

namespace Maxim\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class GreetingController
{
    public function index()
    {
        return new JsonResponse(['Hi there!' => 'Go to books for example.']);
    }
}