<?php

namespace Maxim\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Maxim\Service\Book;
use Maxim\Service\PostgresConnection;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BookController
 * @package Maxim\Controller
 */
class BookController extends FOSRestController
{
    protected $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    /**
     * Creates a Book
     * @Rest\Post("/books")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        return new JsonResponse(["number" => $this->book->produceUpdate($request)]);
    }

    /**
     * Returns a Book list (actually migrates our tables)
     *
     * @Rest\Get("/books")
     */
    public function index()
    {
        return new JsonResponse($this->book->list());
    }
}