<?php

namespace Maxim\Service;

use Exception;
use Maxim\Events\BookUpdate;
use Maxim\Producer\BookUpdateProducer;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use stdClass;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Book
 * @package Maxim\Service
 */
class Book
{
    /**
     * @var PostgresConnection
     */
    protected $connection;

    /**
     * @var BookUpdateProducer
     */
    protected $producer;


    /**
     * Book constructor.
     * @param PostgresConnection $connection
     * @param BookUpdateProducer $producer
     */
    public function __construct(PostgresConnection $connection, BookUpdateProducer $producer)
    {
        $this->connection = $connection;
        $this->producer = $producer;
    }

    /**
     * Produce message
     *
     * @param Request $request
     * @return int
     * @throws Exception
     */
    public function produceUpdate(Request $request): int
    {
        $messageId = $this->makeMessageId();
        $book = new stdClass;
        $book->name = $request->get('name');
        $book->description = $request->get('description');
        $book->author = $request->get('author');
        $book->category = $request->get('category');
        $jsonEncodedBook = json_encode($book);
        $this->producer->publish(
            $jsonEncodedBook,
            '',
            [],
            ['messageId' => $messageId]
        );
        return $messageId;
    }

    /**
     * Consume message
     *
     * @param AMQPMessage $message
     */
    public function consumeUpdate(AMQPMessage $message)
    {
        $book = json_decode($message->getBody());
        $messageInfo = $message->get('application_headers');
        $messageId = $messageInfo->getNativeData()['messageId'];

        $this->update($book);
        $this->setMessageSent($messageId);
    }

    /**
     * Creates and returns id for new message
     *
     * @return int
     * @throws Exception
     */
    public function makeMessageId(): int
    {
        $response = $this->connection->query('
          INSERT INTO messages (status) 
          VALUES (false) RETURNING id');
        $return = $response->fetch();
        if($return['id'] <= 0){
            throw new Exception('Message id is not created');
        }
        return (int) $return['id'];
    }

    /**
     * Set status 'true' for secific message
     *
     * @param int $id
     */
    public function setMessageSent(int $id): void
    {
        $sql = 'UPDATE messages SET status=:status WHERE id=:id';
        $stmt= $this->connection->prepare($sql);
        $stmt->execute(['id' => $id, 'status' => true]);
    }

    /**
     * Returns status for specific message
     *
     * @param int $id
     * @return bool
     */
    public function getMessageStatus(int $id): bool
    {
        $stmt = $this->connection->prepare('
          select status
          from messages
          where id = :id
          limit 1');
        $stmt->execute(['id' => $id]);
        $result = $stmt->fetch();
        return (bool) $result["status"];
    }

    /**
     * Migrates our 2 tables
     *
     * @return void
     */
    public function list()
    {
        $createSql = <<<EOT
CREATE TABLE IF NOT EXISTS books (
	id serial,
	name VARCHAR(255),
	description text,
	author VARCHAR(255),
	category VARCHAR(255),
	PRIMARY KEY( id )
);

CREATE TABLE IF NOT EXISTS messages (
	id serial,
	status bool default false
);
EOT;
        $this->connection->exec($createSql);
    }

    /**
     * Very hard update
     *
     * @param $book
     */
    protected function update($book): void
    {
        $query = '
          INSERT INTO books (name, author, category, description) 
          VALUES (:name, :author, :category, :description)';

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':author', $author);
        $stmt->bindParam(':category', $category);
        $stmt->bindParam(':description', $description);

        $name = $book->name;
        $author = $book->author;
        $category = $book->category;
        $description = $book->description;

        $stmt->execute();
    }
}