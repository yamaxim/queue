<?php

namespace Maxim\Service;

use PDO;
use PDOException;

/**
 * Class PostgresConnection
 * @package Maxim\Service
 */
class PostgresConnection extends PDO
{
    /**
     * PostgresConnection constructor.
     */
    public function __construct()
    {
        try {
            parent::__construct(
                'pgsql:dbname=books;host=postgres',
                'schoolboy',
                'very_secure'
            );

            $this->setAttribute(
                self::ATTR_ERRMODE,
                self::ERRMODE_EXCEPTION
            );
        } catch (PDOException $e) {
            throw new PDOException("Cannot connect to database");
        }
    }
}