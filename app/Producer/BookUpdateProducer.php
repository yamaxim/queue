<?php

namespace Maxim\Producer;

use OldSound\RabbitMqBundle\RabbitMq\Producer;

/**
 * Stub to specify producer in DI
 *
 * Class BookUpdateProducer
 * @package Maxim\Producer
 */
class BookUpdateProducer extends Producer
{

}