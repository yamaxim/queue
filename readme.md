# Install
* docker-compose up -d --build

# Composer
* docker-compose exec php-fpm composer install

# Console usage
* docker-compose exec php-fpm php ./bin/console.php

# Queue
* Стартуем консьюмера: docker-compose exec php-fpm php ./bin/console.php rabbitmq:consumer book_update -vvv
* Тут запуск миграций таблиц http://0.0.0.0:8888/api/books
* Документация swagger тут: 0.0.0.0:8181

# Благодарности
* Хорошему настроению